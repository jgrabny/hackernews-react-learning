import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Search extends Component {
  componentDidMount() {
    if (this.input) {
      this.input.focus();
    }
  }

  render() {
    const { value, onSearchChange, children, onSubmit } = this.props;

    return (
      <form onSubmit={onSubmit}>
        <input
          style={{ marginLeft: '5px' }}
          type="text"
          value={value}
          onChange={onSearchChange}
          ref={node => {
            this.input = node;
          }}
        />
        <button type="submit">{children}</button>
      </form>
    );
  }
}

Search.propTypes = {
  value: PropTypes.string,
  onSearchChange: PropTypes.func.isRequired,
  children: PropTypes.node,
  onSubmit: PropTypes.func.isRequired,
};

export default Search;
