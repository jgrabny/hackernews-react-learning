import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

const Sort = ({
  sortKey, activeSortKey, onSort, children,
}) => {
  const sortClass = ['button-inline'];

  if (sortKey === activeSortKey) {
    sortClass.push('button-active');
  }

  return (
    <Button className={sortClass.join(' ')} onClick={() => onSort(sortKey)}>{children}</Button>
  );
};

Sort.propTypes = {
  sortKey: PropTypes.string.isRequired,
  activeSortKey: PropTypes.string.isRequired,
  onSort: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Sort;
